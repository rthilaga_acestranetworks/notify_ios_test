var app_id='WO_CFoA_O1tb23feBFTsK8u-r';
var model={};
var device_token={};
var backbutton=0;
var set={};
var get_token='';

angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform,$http) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
alert('test');

 model=ionic.Platform.device();
 alert(JSON.stringify(model));


 // var push = PushNotification.init({"ios": {"alert": "true", "badge": "true", "sound": "true","clearBadge": true}});
  
  window.plugins.PushbotsPlugin.initialize("5af6c4721db2dc18607dac69", {"android":{"sender_id":"818195329011"}});

  // Only with First time registration
  window.plugins.PushbotsPlugin.on("registered", function(token){
    alert("Registration Id:" + token);
  });

  //Get user registrationId/token and userId on PushBots, with evey launch of the app even launching with notification
  window.plugins.PushbotsPlugin.on("user:ids", function(data){
    alert("user:ids" + JSON.stringify(data));
  });

  // Should be called once app receive the notification [foreground/background]
window.plugins.PushbotsPlugin.on("notification:received", function(data){
  alert("received:" + JSON.stringify(data));
  
  //iOS: [foreground/background]
  alert("notification received from:" + data.cordova_source);
  //Silent notifications Only [iOS only]
  //Send CompletionHandler signal with PushBots notification Id
  window.plugins.PushbotsPlugin.done(data.pb_n_id);
});

window.plugins.PushbotsPlugin.on("notification:clicked", function(data){
  // var userToken = data.token; 
       // var userId = data.userId;
    alert("clicked:" + JSON.stringify(data));
});

//Reset Badge
window.plugins.PushbotsPlugin.resetBadge();
//Set badge
window.plugins.PushbotsPlugin.setBadge(10);
//Increment badge count
window.plugins.PushbotsPlugin.incrementBadgeCountBy(1);
//Decrement badge count
window.plugins.PushbotsPlugin.decrementBadgeCountBy(10);


    // push.on('registration', function(data) {
    //     alert(JSON.stringify(data));
 /*   device_token=data.registrationId;
    get_token=JSON.parse(localStorage.getItem('device_token'));
    if(get_token != device_token || !get_token || get_token=='' || get_token==null){
        localStorage.setItem('device_token',JSON.stringify(device_token));
        var obj={'app_id':app_id,'device_token':device_token,'model':model.model,'platform':model.platform,'status':true}

        alert(JSON.stringify(obj));

      $http.post('https://push.mylaporetoday.in/device_register', obj).then(function(result) {
        alert(JSON.stringify(result));
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        alert(JSON.stringify(error));

        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
      }*/

    // });

    // push.on('error', function (error) { 
    //   alert(JSON.stringify(error));

    // });

 /*   push.on('notification', function(data) {
       alert(JSON.stringify(data));
    });*/






// window.plugins.PushbotsPlugin.initialize("5af67cfd1db2dc127a25e3ec", {"android":{"sender_id":"8060007986"}});



// window.plugins.PushbotsPlugin.on("registered", function(token){
//   console.log("Registration Id:" + token);
//   alert("Registration Id:" + JSON.stringify(token))
// });

// //Get user registrationId/token and userId on PushBots, with evey launch of the app even launching with notification
// window.plugins.PushbotsPlugin.on("user:ids", function(data){
//   console.log("user:ids" + JSON.stringify(data));
//   alert("user:ids" + JSON.stringify(data))
// });




    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.factory('acepush', function(service) {
  return {
    notification: function(obj) {
      service.post('http://push.mylaporetoday.in/device_register', obj).then(function(result) {
        if (result.status == "success") {
        } else if (result.status == "error") {
          console.log(result.message);
        }
      }, function(error) {
        if (error == 500 || error == 404 || error == 0) {
          console.log("Push Api" + error);
        }
      });
    },
  };
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.playlists', {
      url: '/playlists',
      views: {
        'menuContent': {
          templateUrl: 'templates/playlists.html',
          controller: 'PlaylistsCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/playlists');
});
